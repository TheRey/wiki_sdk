# Afficher un choix

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

![tip](info "Cette page n'indique que la méthode d'affichage de choix via RPG Maker XP.")

Sous PSDK il existe deux méthodes pour afficher un choix. La méthode classique, qui doit toujours être utilisée lorsque vous avez entre 2 et 4 options et la méthode script qui doit être utilisé si vous avez plus que 4 options.

Bien entendu, les choix ont des configurations et des codes spécifiques.

## Configurer les choix

L'interrupteur `26` "**Choix en haut**" affiche le choix en haut à gauche à la manière des distributeurs de boisson.

## Afficher un choix simple

Pour afficher un choix simple, utilisez la commande de message pour expliquer le choix et ensuite la commande d'affichage de choix pour effectuer le choix.

Cette commande de choix permet d'autoriser l'annulation. L'annulation du point de vue du joueur consiste à appuyer sur la touche retour. Selon les paramètres que vous avez choisi, cela peut prendre un choix spécifique ou réaliser une action spécifique. Si vous n'autorisez pas l'annulation, le joueur sera contraint de choisir.

![tip](danger "Ne mettez jamais de commandes entre le message et le choix. Ceci risque de casser le choix et de l'afficher dans le vide.")

## Afficher des couleurs dans les choix

Chaque option peut avoir sa propre couleur, il vous suffit pour cela d'entrer le même code que pour les messages `\c[x]` où `x` correspond au code couleur.

## Utiliser un texte dynamique (RH & CSV)

Les textes dynamique fonctionnent de la même manière que dans les [Messages](messages.html#cat_10 "Afficher un message provenant du fichier Texte de Ruby Host")

![tip](info "Pour les choix Oui / Non, le texte dynamique de Oui est `\t[11,27]` et celui de Non est `\t[11,28]`.")

## Afficher un choix avec plus de 4 options

Pour afficher un choix ayant plus de 4 options vous devez utiliser la commande de script [`choice`](https://psdk.pokemonworkshop.fr/yard/Interpreter.html#choice-instance_method) et ensuite utiliser la commande d'affichage de message qui décrit le choix. Une fois le choix exécuté, utilisez les commandes de condition pour inspecter la variable qui contient le résultat du choix (selon les paramètres de votre commande).

![tip](warning "Attention, contrairemet à la commande d'évènement qui se met après le message, la commande de script se met avant le message.")

