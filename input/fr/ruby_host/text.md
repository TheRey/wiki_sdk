# Editeur de Textes

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

## Interface des Textes de RubyHost

Le bouton `Textes` accessible sur l'interface d'accueil de RubyHost permet d'accéder à une fenêtre d'édition des textes utilisés par tous les systèmes (combat, magasin, etc.).

![Interface|center](img/ruby_host/Rubyhost_textmain.png "Interface d'édition des textes")

L'édition de ces textes est possible dans plusieurs langues :
- Le japonais (sans les Kanjis)
- L’allemand
- L’anglais
- L’espagnol
- Le français
- L’italien
- Le coréen

![tip](info "Le développeur aura tout de même à traduire les messages de son jeu, ainsi que les données additionnelles.")

## Modifier les textes

Pour modifier les données de texte désirées, il suffit de suivre ces étapes :

1. Dans la liste déroulante `Fichier :`, choisissez l'entrée X qui vous intéresse.
    - Si vous souhaitez ajouter un texte :
        - Dans `ID :`, rentrez le nombre contenu dans la case `ID MAX :` puis cliquez sur `Aller`.
        - Cliquez sur `Ajouter un texte`.
        - Remplissez les données correspondant aux différentes langues d'affichage.
        - Cliquez sur `Valider l'édition`.  
            ![tip](info "Il est nécessaire de valider l'édition pour bien enregistrer les modifications, sinon elles seront perdues")
        - Cliquez sur `Sauvegarder`.
    - Si vous souhaitez modifier un texte :
        - Dans `ID :`, rentrez le nombre correspondant au texte à modifier puis cliquez sur `Aller`.
        - Modifiez les données correspondant aux différentes langues d'affichage.
        - Cliquez sur `Valider l'édition`.  
            ![tip](info "Il est nécessaire de valider l'édition pour bien enregistrer les modifications, sinon elles seront perdues")
        - Cliquez sur `Sauvegarder`.
    - Si vous souhaitez supprimer un texte (seulement possible avec le dernier texte de l'entrée sélectionnée) :
        - Cliquez sur `Supprimer le dernier`.
