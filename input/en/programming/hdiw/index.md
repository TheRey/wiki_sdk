# How does it work

This section of the wiki is about how things work in PSDK programmatively speaking.

## Index

The index is not definitive, we'll add content when it'll be asked (and written) but currently here's the actual index:

- [Scenes (GamePlay)](en/programming/hdiw/scenes.md)
- [PSDK UI System](en/programming/hdiw/ui.md)